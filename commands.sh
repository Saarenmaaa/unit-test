#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test

git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

# Rewrites .gitignore
echo "node_modules" > .gitignore

#git remote add origin https://gitlab.com/Saarenmaaa/unit-test
#git remote set-url origin
#git push -u origin master

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

node src/main.js
node --watch-path=src src/main.js

#Run
npm run dev
#Test
npm run test

# test files that goes to staging area
git add . --dry-run

git push --set-upstream origin master